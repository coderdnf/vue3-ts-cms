# vue3-ts-cms

> 使用 Vue3 + TS + Element-Plus 搭建的后台管理系统
>
> 想要学习 Vue3 + TS 的朋友可以购买 coderwhy 的课程
>
> https://ke.qq.com/course/3453141

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```
